#ifndef Vec2f_H
#define Vec2f_H

#include <cmath>
#include <string>

class Vec2f {
	public:
		float x;
		float y;
		
		//constructors
		Vec2f(void);
		Vec2f(float, float);
		
		float magnitude(void);
		Vec2f normalize(void);
		
		//operator overloading
		//addition / subtraction
		Vec2f operator+(Vec2f);
		Vec2f operator-(Vec2f);
		Vec2f operator+(float);
		Vec2f operator-(float);
		Vec2f operator+=(Vec2f);
		Vec2f operator-=(Vec2f);
		Vec2f operator+=(float);
		Vec2f operator-=(float);
		
		//scaling
		Vec2f operator*(float);
		Vec2f operator/(float);
		Vec2f operator*=(float);
		Vec2f operator/=(float);
		
		//dot
		float operator*(Vec2f);
		//cross
		float operator/(Vec2f);
		
		//comparators
		bool operator==(Vec2f);
		bool operator!=(Vec2f);
		
		//negate
		Vec2f operator-(void);
		
		//averaging by proportion (just generally useful)
		static Vec2f mix(Vec2f,Vec2f,float);
};

#endif
