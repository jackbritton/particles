#include "Particle.hpp"

Particle::Particle(void) {
	//defaults
    Particle::age = 0;
}

void Particle::update(float d) {
	Particle::velocity += Particle::acceleration * d;
	Particle::position += Particle::velocity * d;
    Particle::age += d;
}
