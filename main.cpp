#include <stdio.h>
#include <vector>
#include <SFML/Graphics.hpp>
//#include <GL/glew.h>

#include "Vec2f.hpp"
#include "Color.hpp"
#include "Particle.hpp"

//global
std::vector<Particle> particles;

float randomNormal(void) {
	return static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
}

void spawnParticlesUniform(Vec2f position, float velocity, int n) {
	int size = particles.size();
	float a = 2 * M_PI / n;
	for (size_t i = 0; i < n; i++) {
		particles.push_back(Particle());
		
		particles[i + size].position = position;
		float r = velocity;
		float angle = a * i;
		particles[i + size].velocity = Vec2f(r * cos(angle), r * sin(angle));
		
		particles[i + size].size = Vec2f(2,2);
	}
}
void spawnParticlesRandom(Vec2f position, float velocity, int n) {
	int size = particles.size();
	float a = 2 * M_PI / n;
	for (size_t i = 0; i < n; i++) {
		particles.push_back(Particle());
		
		particles[i + size].position = position;
		float r = velocity;
		float a = randomNormal() * 2 * M_PI;
		float x = r * cos(a);
		float y = r * sin(a);
		particles[i + size].velocity = Vec2f(x,y);
		
		particles[i + size].size = Vec2f(2,2);
	}
}

int main(void) {
	
	sf::RenderWindow window(sf::VideoMode::getDesktopMode(), "");
	sf::Clock deltaClock;
	sf::Clock clock;
	
	//init
	Vec2f windowSize = Vec2f(window.getSize().x, window.getSize().y);

	while (window.isOpen()) {
		sf::Event event;
		while (window.pollEvent(event)) {
			if (event.type == sf::Event::Closed)
				window.close();
		}

		//update everything here...
		
		float delta = deltaClock.restart().asSeconds();
		
		//mouse / key functions
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
			Vec2f mousePosition = Vec2f(sf::Mouse::getPosition(window).x, sf::Mouse::getPosition(window).y);
			spawnParticlesRandom(mousePosition, 32, 16);
		}
		if (sf::Mouse::isButtonPressed(sf::Mouse::Middle)) {
			Vec2f mousePosition = Vec2f(sf::Mouse::getPosition(window).x, sf::Mouse::getPosition(window).y);
			spawnParticlesUniform(mousePosition, 128, 16);
		}
		//k kills all particles
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::K)) {
			particles.erase(particles.begin(),particles.end());
		}
		//g increases the gravity scalar
		float gravityScalar = 64;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::G)) {
			gravityScalar = 1024;
		}
		//s slows all particles
		//right mouse button sets the impulse target
		Vec2f impulseTarget;
		if (sf::Mouse::isButtonPressed(sf::Mouse::Right)) {
			impulseTarget = Vec2f(sf::Mouse::getPosition(window).x, sf::Mouse::getPosition(window).y);
		} else {
			for (size_t i = 0; i < particles.size(); i++) {
				impulseTarget += particles[i].position;
			}
			impulseTarget /= particles.size();
		}
		
		int size = particles.size();
		float elapsedTime = clock.getElapsedTime().asSeconds();
        
        for (size_t i = 0; i < size; i++) {
            Particle &p = particles[i];
            p.acceleration = (impulseTarget - p.position).normalize() * gravityScalar;
            //friction
            p.acceleration += -p.velocity * 0.05;
            p.update(delta);
            
            //colors
            float proportion = particles[i].position.y / windowSize.y;
            Color color1 = Color::mix(Color(0,0,255),Color(255,0,0),(sin(elapsedTime * M_PI) + 1)/2);
            Color color2 = Color::mix(Color(255,0,255),Color(0,255,0),(sin(elapsedTime) + 1)/2);
            p.color = Color::mix(color1,color2,proportion);
        }

		window.clear(sf::Color::Black);

		//draw everything here...
		//basically porting our custom classes in Particle to sfml
		for (size_t i = 0; i < size; i++) {
			sf::RectangleShape s;
			Particle p = particles[i];
			s.setPosition(sf::Vector2f(p.position.x, p.position.y));
			s.setSize(sf::Vector2f(p.size.x, p.size.y));
			s.setFillColor(sf::Color(p.color.r, p.color.g, p.color.b));
			window.draw(s);
		}

		window.display();
	}

	return 0;
}
