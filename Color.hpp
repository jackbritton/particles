#ifndef Color_H
#define Color_H

class Color {
	public:
		int r;
		int g;
		int b;
		
		//constructors
		Color(void);
		Color(int,int,int);
		
		//mix
		static Color mix(Color,Color,float);
};

#endif
