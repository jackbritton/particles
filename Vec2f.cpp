#include "Vec2f.hpp"

//constructors
Vec2f::Vec2f(void) {
	Vec2f::x = 0;
	Vec2f::y = 0;
}
Vec2f::Vec2f(float x, float y) {
	Vec2f::x = x;
	Vec2f::y = y;
}

//magnitude / normalizing
float Vec2f::magnitude(void) {
	return sqrt(pow(Vec2f::x, 2) + pow(Vec2f::y, 2));
}
Vec2f Vec2f::normalize(void) {
	float mag = Vec2f::magnitude();
	if (mag == 0.0) {
		return Vec2f();
	}
	return Vec2f(Vec2f::x/mag,Vec2f::y/mag);
}

//addition / subtraction
Vec2f Vec2f::operator+(Vec2f b) {
	return Vec2f(Vec2f::x + b.x, Vec2f::y + b.y);
}
Vec2f Vec2f::operator-(Vec2f b) {
	return Vec2f(Vec2f::x - b.x, Vec2f::y - b.y);
}
Vec2f Vec2f::operator+(float b) {
	return Vec2f(Vec2f::x + b, Vec2f::y + b);
}
Vec2f Vec2f::operator-(float b) {
	return Vec2f(Vec2f::x - b, Vec2f::y - b);
}
Vec2f Vec2f::operator+=(Vec2f b) {
	Vec2f::x += b.x;
	Vec2f::y += b.y;
}
Vec2f Vec2f::operator-=(Vec2f b) {
	Vec2f::x -= b.x;
	Vec2f::y -= b.y;
}
Vec2f Vec2f::operator+=(float b) {
	Vec2f::x += b;
	Vec2f::y += b;
}
Vec2f Vec2f::operator-=(float b) {
	Vec2f::x -= b;
	Vec2f::y -= b;
}

//scaling
Vec2f Vec2f::operator*(float b) {
	return Vec2f(Vec2f::x * b, Vec2f::y * b);
}
Vec2f Vec2f::operator/(float b) {
	return Vec2f(Vec2f::x / b, Vec2f::y / b);
}
Vec2f Vec2f::operator*=(float b) {
	Vec2f::x *= b;
	Vec2f::y *= b;
}
Vec2f Vec2f::operator/=(float b) {
	Vec2f::x /= b;
	Vec2f::y /= b;
}

//dot
float Vec2f::operator*(Vec2f b) {
	return Vec2f::x * b.x + Vec2f::y * b.y;
}
//cross
float Vec2f::operator/(Vec2f b) {
	return Vec2f::x * b.y - Vec2f::y * b.x;
}

//comparators
bool Vec2f::operator==(Vec2f b) {
	if (Vec2f::x == b.x && Vec2f::y == b.y) {
		return true;
	}
	return false;
}
bool Vec2f::operator!=(Vec2f b) {
	if (Vec2f::x == b.x && Vec2f::y == b.y) {
		return false;
	}
	return true;
}

//negate
Vec2f Vec2f::operator-(void) {
	return Vec2f(-Vec2f::x, -Vec2f::y);
}

//mixing
Vec2f Vec2f::mix(Vec2f a, Vec2f b, float p) {
	Vec2f c;
	c.x = (a.x - b.x) * p + b.x;
	c.y = (a.y - b.y) * p + b.y;
	return c;
}
