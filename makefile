CC=g++
CFLAGS=-lsfml-graphics -lsfml-window -lsfml-system
TARGET=particles

main :
	$(CC) $(CFLAGS) *.cpp -o $(TARGET)
