#ifndef Particle_H
#define Particle_H

#include "Vec2f.hpp"
#include "Color.hpp"

class Particle {
	public:
		//basic properties
		Vec2f position;
		Vec2f velocity;
		Vec2f acceleration;
		Vec2f size;
		Color color;

        //lifetime
        float age;
		
		//constructors
		Particle(void);
		
		//update
		void update(float);
};

#endif
