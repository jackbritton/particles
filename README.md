The first C++ / SFML project I got really deep into. Left and middle mouse button to spawn different kinds of particle showers, right click to set the attraction point at your mouse. To date, probably the prettiest project I've done.

Compiling on Linux / Mac OS X should be easy. Just install the newest version of SFML and run make. For Mac, I recommend installing SFML with homebrew. For Windows, you'd need to rewrite some stuff.. this was just a fun little project for me, so I have no plans of doing that.
