#include "Color.hpp"

Color::Color(void) {
	Color::r = 0;
	Color::g = 0;
	Color::b = 0;
}
Color::Color(int r, int g, int b) {
	Color::r = r;
	Color::g = g;
	Color::b = b;
}

Color Color::mix(Color a, Color b, float p) {
	Color c;
	c.r = (a.r - b.r) * p + b.r;
	c.g = (a.g - b.g) * p + b.g;
	c.b = (a.b - b.b) * p + b.b;
	return c;
}
